<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
ini_set('error_reporting', E_ALL);
define("TEMPLATES_DIR", __DIR__ . '/templates');

$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' && strlen($_SERVER['HTTPS']) > 0) ? 'https' : 'http';
define("APP_URL", $protocol . '://' .$_SERVER['SERVER_NAME']);

//echo 'app url ' . APP_URL;

//print_r($_SERVER);

require('./db/db.inc.php');
require('./utils/SimpleRouter.php');
require('./models/addressbook.php');

$simpleRouter = new SimpleRouter();
$simpleRouter->parse();
//print_r($simpleRouter);
//echo $simpleRouter->controller;
$controllerClass = $simpleRouter->controller;
//echo './controllers/' . $controllerClass . '.php';
require('./controllers/' . $controllerClass . '.php');
require('./repository/AdressbookRepository.php');


$controllerMethod = $simpleRouter->method;
$controllerParams = $simpleRouter->params;
$controller = new $controllerClass();

//print_r($controllerMethod);

$content = $controller->$controllerMethod($controllerParams);

$mainTemplate = file_get_contents('templates/base.tpl');

$output = str_replace(["{{CONTENT}}", "{{app_url}}"], [$content, APP_URL], $mainTemplate);

echo $output;
