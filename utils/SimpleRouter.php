<?php

/**
 * Class SimpleRouter
 *
 * very simplified, raw and not complete implementation of router
 * binds controllers class/method to route APP_URL/index.php/alias_to_class_method
 */
class SimpleRouter
{
    public $routes = [
        'main' => [
            'controller' => 'AdressbookController',
            'method' => 'index'
        ],
        'list' => [
            'controller' => 'AdressbookController',
            'method' => 'index'
        ],
        'add' => [
            'controller' => 'AdressbookController',
            'method' => 'add'
        ],
        'delete' => [
            'controller' => 'AdressbookController',
            'method' => 'delete'
        ],
        'edit' => [
            'controller' => 'AdressbookController',
            'method' => 'edit'
        ]
    ];

    public $controller;

    public $method;

    public $params = [];

    /**
     * parses url, finds responsible controller and method
     */
    public function parse()
    {
        $protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' && strlen($_SERVER['HTTPS']) > 0) ? 'https' : 'http';
        $uri = $protocol . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        //echo 'uri ' . $uri;
        $uriCleaned = str_replace(APP_URL, '', $uri);
        //echo 'uricleaned '. $uriCleaned;
        $aUri = explode('/', $uriCleaned);

        if(isset($aUri[2]) && isset($this->routes[$aUri[2]])){
            $this->controller = $this->routes[$aUri[2]]['controller'];
            $this->method = $this->routes[$aUri[2]]['method'];
            $paramCounter = 3;
            // other possible parameters
            while(isset($aUri[$paramCounter])){
                $this->params[] = $aUri[$paramCounter];
                $paramCounter ++;
            }
        }else{
            $this->controller = $this->routes['main']['controller'];
            $this->method = $this->routes['main']['method'];
        }
       //echo($this->method);



    }


}
