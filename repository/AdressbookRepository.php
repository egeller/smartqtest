<?php

/**
 * Class AdressbookRepository
 *
 * all db operationen with addressbook entity
 */
class AdressbookRepository
{

    /**
     * saves addressbook entry in DB
     *
     * @param $entry addressbook adressbook instance
     */
    public function save(addressbook $entry)
    {
        global $pdo;
        $firstName = $entry->getFirstName();
        $secondName = $entry->getSecondName();
        $phone = $entry->getPhone();
        $mobile = $entry->getMobile();
        $zip = $entry->getZip();
        $street = $entry->getStreet();
        $city = $entry->getCity();
        $state = $entry->getState();
        $houseNumber = $entry->getHouseNumber();

        $statement = $pdo->prepare("INSERT INTO addressbook (
        first_name, 
        second_name, 
        phone,
        mobile,
        zip,
        street, 
        city,
        state,
        house_number) 
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        if($statement->execute(array($firstName,
            $secondName,
            $phone,
            $mobile,
            $zip,
            $street,
            $city,
            $state,
            $houseNumber))){
            return true;
        }else{
            return false;
        }

    }

    /**
     * updates addressbook entry
     *
     * @param $entry addressbook  addressbook entry
     */
    public function update($entry)
    {
        global $pdo;
        //echo 'to update';
        //print_r($entry);
        $id = $entry->getId();
        $firstName = $entry->getFirstName();
        $secondName = $entry->getSecondName();
        $phone = $entry->getPhone();
        $mobile = $entry->getMobile();
        $zip = $entry->getZip();
        $street = $entry->getStreet();
        $city = $entry->getCity();
        $state = $entry->getState();
        $houseNumber = $entry->getHouseNumber();

        $statement = $pdo->prepare("update addressbook set
        first_name=?, 
        second_name=?, 
        phone=?,
        mobile=?,
        zip=?,
        street=?, 
        city=?,
        state=?,
        house_number=?
        where id=?");
        if($statement->execute(array($firstName,
            $secondName,
            $phone,
            $mobile,
            $zip,
            $street,
            $city,
            $state,
            $houseNumber,
            $id))){
            //echo 'execured';
            return true;
        }else{
            return false;
        }


    }

    /**
     * deletes addressbook entry
     *
     * @param $id int ID of addressbook entry
     */
    public function delete($id)
    {
        global $pdo;
        $statement = $pdo->prepare("delete FROM addressbook WHERE id = :id");
        if($statement->execute(array(':id' => $id))){
            return true;
        }else{
            return false;
        }

    }

    /**
     * finds addressbook entry by id
     *
     * @param $id
     * @return addressbook
     */
    public function getEntryById($id){
        global $pdo;
        $entry = new addressbook();
        $statement = $pdo->prepare("SELECT * FROM addressbook WHERE id = :id");
        if($statement->execute(array(':id' => $id))){
            while($row = $statement->fetch()) {
                $entry->setId($row['id']);
                $entry->setFirstName($row['first_name']);
                $entry->setSecondName($row['second_name']);
                $entry->setState($row['state']);
                $entry->setPhone($row['phone']);
                $entry->setMobile($row['mobile']);
                $entry->setStreet($row['street']);
                $entry->setCity($row['city']);
                $entry->setHouseNumber($row['house_number']);
            }

        }

        return $entry;

    }

    /**
     * get all addressbook entries
     */
    public function getAll($orderField = 0, $orderDirection = 0)
    {
        global $pdo;
        $aContacts = [];
        $query = "SELECT * FROM addressbook";
        if($orderField !== 0){
            switch ($orderField) {
                case 1:
                    $query .= " order by first_name ";
                    break;
                case 2:
                    $query .= " order by second_name ";
                    break;
                case 3:
                    $query .= " order by city ";
                    break;
                case 4:
                    $query .= " order by phone ";
                    break;

            }

            if($orderDirection == 2){
                $query .= " desc ";
            }

        }

        //echo $query;

        $statement = $pdo->prepare($query);


        if($statement->execute()) {
            while($row = $statement->fetch()) {
                $entry = new addressbook();
                $entry->setId($row['id']);
                $entry->setFirstName($row['first_name']);
                $entry->setSecondName($row['second_name']);
                $entry->setState($row['state']);
                $entry->setPhone($row['phone']);
                $entry->setMobile($row['mobile']);
                $entry->setStreet($row['street']);
                $entry->setCity($row['city']);
                $entry->setZip($row['zip']);
                $entry->setHouseNumber($row['house_number']);
                $aContacts[] = $entry;
            }
        } else {

            echo "SQL Error <br />";
            echo $statement->queryString."<br />";
            echo $statement->errorInfo()[2];

        }

        return $aContacts;

    }


}
