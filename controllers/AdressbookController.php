<?php

/**
 * Class AdressbookController
 *
 * Controller for Adressbook routes : create, update, index (list), etc.
 */
class AdressbookController
{

    /**
     * shows list of entries
     *
     * @param $aParams
     * @return string|string[]
     */
    public function index($aParams)
    {
        //print_r($aParams);

        $orderDirection = isset($aParams[1]) ? $aParams[1] : 0;
        $orderField = isset($aParams[0]) ? $aParams[0] : 0;


        $repository = new AdressbookRepository();
        $aContacts = $repository->getAll($orderField, $orderDirection);
        $rowTpl = file_get_contents(TEMPLATES_DIR . '/adressbookrows.tpl');
        $rows = '';
        foreach ($aContacts as $entry) {
            $id = $entry->getId();
            $firstName = $entry->getFirstName();
            $secondName = $entry->getSecondName();
            $phone = $entry->getPhone();
            $mobile = $entry->getMobile();
            $zip = $entry->getZip();
            $street = $entry->getStreet();
            $city = $entry->getCity();
            $state = $entry->getState();
            $houseNumber = $entry->getHouseNumber();
            $rows .= str_replace(['{{id}}',
                '{{firstName}}',
                '{{secondName}}',
                '{{street}}',
                '{{houseNumber}}',
                '{{zip}}',
                '{{city}}',
                '{{state}}',
                '{{phone}}',
                '{{mobile}}',
                '{{app_url}}'

            ],
                [$id,
                    $firstName,
                    $secondName,
                    $street,
                    $houseNumber,
                    $zip,
                    $city,
                    $state,
                    $phone,
                    $mobile,
                    APP_URL

                ]
                , $rowTpl
            );
        }

        $indexTpl = file_get_contents(TEMPLATES_DIR . '/index.tpl');
        $content = str_replace(['{{ROWS}}', '{{app_url}}'], [$rows, APP_URL], $indexTpl);
        return $content;

    }

    /**
     * adds addressbook entry
     *
     * @return false|string|string[]
     */
    public function add()
    {
        $errors = '';
        $entry = new addressbook();

        $repository = new AdressbookRepository();
        if (isset($_POST['add']) && intval($_POST['add']) == 42) {

            $entry->setFirstName($_POST['first_name']);
            $entry->setSecondName($_POST['second_name']);
            $entry->setStreet($_POST['street']);
            $entry->setHouseNumber($_POST['house_number']);
            $entry->setPhone($_POST['phone']);
            $entry->setMobile($_POST['mobile']);
            $entry->setCity($_POST['city']);
            $entry->setState($_POST['state']);
            $entry->setZip($_POST['zip']);

            if (trim($entry->getSecondName()) == '' || trim($entry->getFirstName()) == '') {
                $errors = 'First Name and Second Name have to be filled';
            }
            if (strlen($errors) == 0) {
                if ($repository->save($entry)) {
                    header('Location: ' . APP_URL);

                } else {
                    echo 'fehler';
                }

            }

        }
        $content = file_get_contents(TEMPLATES_DIR . '/entry_add.tpl');
        $content = str_replace(
            [
                '{{ERRORS}}',

                '{{headline}}',
                '{{app_url}}',
                '{{first_name}}',
                '{{second_name}}',
                '{{street}}',
                '{{house_number}}',
                '{{city}}',
                '{{state}}',
                '{{zip}}',
                '{{phone}}',
                '{{mobile}}'
            ],
            [
                $errors,
                'Add new entry',
                APP_URL,
                $entry->getFirstName(),
                $entry->getSecondName(),
                $entry->getStreet(),
                $entry->getHouseNumber(),
                $entry->getCity(),
                $entry->getState(),
                $entry->getZip(),
                $entry->getPhone(),
                $entry->getMobile()

            ], $content);
        return $content;
    }


    /**
     * edits addressbook entry
     *
     * @param $aParams array Controller Parameters
     *
     * @return false|string|string[]
     */
    public function edit($aParams)
    {
//print_r($aParams);
        $id = $aParams[0];
        $errors = '';

        $repository = new AdressbookRepository();
        $entry = $repository->getEntryById($id);
        //print_r($entry);
        if (isset($_POST['add']) && intval($_POST['add']) == 42) {

            $entry->setFirstName($_POST['first_name']);
            $entry->setSecondName($_POST['second_name']);
            $entry->setStreet($_POST['street']);
            $entry->setHouseNumber($_POST['house_number']);
            $entry->setPhone($_POST['phone']);
            $entry->setMobile($_POST['mobile']);
            $entry->setCity($_POST['city']);
            $entry->setState($_POST['state']);
            $entry->setZip($_POST['zip']);
            //print_r($entry);

            if (trim($entry->getSecondName()) == '' || trim($entry->getFirstName()) == '') {
                $errors = 'First Name and Second Name have to be filled';
            }
            if (strlen($errors) == 0) {
                if ($repository->update($entry)) {
                    //echo 'update';
                    //header('Location: ' . APP_URL);

                } else {
                    //echo 'fehler';
                }

            }

        }
        $content = file_get_contents(TEMPLATES_DIR . '/entry_add.tpl');
        $content = str_replace(
            [
                '{{ERRORS}}',
                '{{headline}}',
                '{{app_url}}',
                '{{first_name}}',
                '{{second_name}}',
                '{{street}}',
                '{{house_number}}',
                '{{city}}',
                '{{state}}',
                '{{zip}}',
                '{{phone}}',
                '{{mobile}}'
            ],
            [
                $errors,
                'Edit entry',
                APP_URL,
                $entry->getFirstName(),
                $entry->getSecondName(),
                $entry->getStreet(),
                $entry->getHouseNumber(),
                $entry->getCity(),
                $entry->getState(),
                $entry->getZip(),
                $entry->getPhone(),
                $entry->getMobile()

            ], $content);
        return $content;
    }

    /**
     * deletes entry
     *
     * @param $aParams array Controller Paramaters
     */
    public function delete($aParams){
        $id = $aParams[0];
        $repository = new AdressbookRepository();
        if($repository->delete($id)){
            header('Location: ' . APP_URL);
        }

    }

}
