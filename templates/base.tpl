<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Addressbook</title>
    <meta name="description" content="Adressbook">
    <link rel="stylesheet" href="{{app_url}}/css/styles.css">

</head>

<body>
{{CONTENT}}

</body>
</html>
