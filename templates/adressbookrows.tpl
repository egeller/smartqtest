<tr>
    <td>{{id}}</td>
    <td>{{firstName}}</td>
    <td>{{secondName}}</td>
    <td>{{street}}</td>
    <td>{{houseNumber}}</td>
    <td>{{zip}}</td>
    <td>{{city}}</td>
    <td>{{state}}</td>
    <td>{{phone}}</td>
    <td>{{mobile}}</td>
    <td><a href="{{app_url}}/index.php/edit/{{id}}">Edit</a></td>
    <td><a href="{{app_url}}/index.php/delete/{{id}}" onclick="return confirm('Do you really want to delete this entry?')">Delete</a></td>
</tr>
