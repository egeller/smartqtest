<h2>{{headline}}</h2>
<form method="post">
<div class="errors">
    {{ERRORS}}
</div>
<div class="form-group">
    <label for="first_name">First Name</label>
    <input type="text" class="form-control" name="first_name" id="first_name" value="{{first_name}}" required>
</div>
    <div class="form-group">
    <label for="second_name">Second Name</label>
    <input type="text" class="form-control" name="second_name" id="second_name" value="{{second_name}}" required>
    </div>
    <div class="form-group">
    <label for="street">Street</label>
    <input type="text" class="form-control" name="street" id="street" value="{{street}}" >
    </div>
    <div class="form-group">
    <label for="house_number">House Number</label>
    <input type="text" class="form-control" name="house_number" id="house_number" value="{{house_number}}" >
    </div>
    <div class="form-group">
    <label for="zip">ZIP</label>

    <input type="text" class="form-control" name="zip" id="zip" value="{{zip}}" >
    </div>
    <div class="form-group">
        <label for="city">City</label>
        <input type="text" class="form-control" name="city" value="{{city}}" id="city">
    </div>
    <div class="form-group">
        <label for="state">State</label>
        <input type="text" class="form-control" value="{{state}}" name="state" id="state">
    </div>
    <div class="form-group">
        <label for="phone">Phone</label>
        <input type="tel" class="form-control" value="{{phone}}" pattern="^\+?\d*$" name="phone" id="phone">
    </div>
    <div class="form-group">
        <label for="mobile">Mobile</label>
        <input class="form-control" type="tel" value="{{mobile}}" pattern="^\+?\d*$" name="mobile" id="mobile">
    </div>
    <div class="form-group">
        <input type="hidden" name="add" value="42">
        <input type="hidden" name="id" value="{{id}}">
        <input type="submit" value="Save"/>
    </div>
</form>
<a href="{{app_url}}">Back</a>
